import React from 'react';
import Spinner from '../../common/images/spinner.svg';

const Loader = () => {
  return (
    <div>
      <img src={Spinner} alt="Loading"/>
    </div>
  )
};

export default Loader;
