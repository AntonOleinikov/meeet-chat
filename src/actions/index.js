import axios from 'axios';
import { history } from '../store';
import * as types from './types';

export const createNewUser = ({email, password}) => dispatch => {
  dispatch({
    type: types.CREATE_USER_LOADING,
    payload: true
  })

  return axios.post(`${process.env.REACT_APP_SOCKET_IO_URL}/auth/sign-up`, {
    email,
    password
  })
  .then(() => {
      dispatch({
        type: types.CREATE_USER_SUCCESS,
        payload: 'Email is valid'
      });
      history.push('/confirm-email');
  })
  .catch(() => {
      dispatch({
        type: types.CREATE_USER_ERROR,
        payload: 'Email is not valid'
      })
   });
}

export const logInUser = ({email, password}) => dispatch => {
  dispatch({
    type: types.LOGIN_USER_LOADING,
    payload: true
  });
  return axios.post(`${process.env.REACT_APP_SOCKET_IO_URL}/auth/login`, {
    email,
    password
  })
  .then(res => {
    dispatch({
      type: types.LOGIN_USER_SUCCESS,
      payload: res.user
    })
    history.push('/chat');
  })
  .catch(() => {
    dispatch({
      type: types.LOGIN_USER_ERROR,
      payload: 'Email or password is invalid'
    })
   });
};
