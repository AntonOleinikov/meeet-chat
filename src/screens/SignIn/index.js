import React, {useState} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {logInUser} from '../../actions';
import {Container, Overlay, LoginForm, Button, Slogan, LoaderContainer, SignInLink, TextField, ErrorMessage} from './Login.styles';
import Logo from '../../common/icons/logo64_64.svg'
import Background from '../../common/images/photos.png';
import Loader from '../../components/Loader';

const SignInPage = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  function registerUser() {

    props.signInUser({
      email,
      password
    })
  }

  const onEmailChange = ({ target }) => {
    setEmail(target.value)
  };

  const onPasswordChange = ({ target }) => {
    setPassword(target.value)
  };


  return (
    <Container
      style={{ backgroundImage: `url('${Background}')`}}
    >
      <Overlay>
        <LoginForm>
          <Slogan>
            <img src={Logo} alt=""/>
            <span>Meet. Chat. Enjoy.</span>
          </Slogan>
          <div>
            <h1>Log in</h1>
            <div style={{ marginBottom: 20 }}>
              <TextField
                onChange={onEmailChange}
                placeholder={'Your email'}
              />
            </div>

            <div>
              <TextField
                type="password"
                onChange={onPasswordChange}
                placeholder={'Your password'}
              />
            </div>

            {
              props.errorMessage && (
                <ErrorMessage>{props.errorMessage}</ErrorMessage>
              )
            }

            <SignInLink>
              <Link to="/sign-in">Forgot password?</Link>
            </SignInLink>

            {
              props.isLoading && (
                <LoaderContainer>
                  <Loader />
                </LoaderContainer>
              )
            }

            <Button
              disabled={!email || !password}
              onClick={registerUser}
            >Log In
            </Button>
          </div>
        </LoginForm>
      </Overlay>
    </Container>
  )

};

const mapDispatchToProps = {
  signInUser: logInUser
};
const mapStateToProps = state => {
  return {
    errorMessage: state.userReducer.errorMessage,
    isLoading: state.userReducer.loading
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage);
