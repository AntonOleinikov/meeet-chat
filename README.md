1. Add `.env` and add the next variables: 

    ```
    REACT_APP_SOCKET_IO_URL
    REACT_APP_PEER_SERVER_URL
    REACT_APP_TURN_PASSWORD
    REACT_APP_TURN_USER
    REACT_APP_TURN_HOST
    ```


2. Run `npm i`

3. Run `npm start`
